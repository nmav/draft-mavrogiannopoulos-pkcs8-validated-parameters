all: draft-mavrogiannopoulos-pkcs8-validated-parameters.txt draft-mavrogiannopoulos-pkcs8-validated-parameters.html

clean:
	rm -f *~ \
	draft-mavrogiannopoulos-pkcs8-validated-parameters.txt draft-mavrogiannopoulos-pkcs8-validated-parameters.html

draft-mavrogiannopoulos-pkcs8-validated-parameters.txt: draft-mavrogiannopoulos-pkcs8-validated-parameters.xml
	xml2rfc $<

draft-mavrogiannopoulos-pkcs8-validated-parameters.html: draft-mavrogiannopoulos-pkcs8-validated-parameters.xml
	xml2rfc --html $<

